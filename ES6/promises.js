console.info('****** PROMISES ******');

function timeout(duration = 0, logString = null) {
    if (logString) {
        console.log(logString);
    } else {
        console.log('Asking for a new Promise');
    }
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration);
        //setTimeout(reject, duration);
    });
}

let p = timeout(2000)
    .then(() => {
        return timeout(3000,"Chiamo di nuovo una promise");
    })
    .then(() => {
        return timeout(3000,"One more promise...");
    })
    .then(() => {
        console.log('Fine della catena delle Promises');
    });