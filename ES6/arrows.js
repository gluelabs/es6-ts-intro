console.info('***** ARROW FUNCTIONS *****');

const _array = ['Apples', 'Pears', 'Bananas'];

console.log("------ Simple iteration (fruits) -----");
// Simple iteration
_array.forEach(v => {
  console.log(v);
});

/*
 * Lexical this
 * Il this è preservato nelle funzioni interne
 * 
 */
let person = {
  _name: 'Bob',
  _friends: ['Alice', 'Charles'],
  printFriends() {
    this._friends.forEach(f =>
      console.log(this._name + " knows " + f));
  },
  anotherPrint: () => {
    console.log(this._friends);
    this._friends.forEach(f =>
      console.log(this._name + " conosce " + f));
  }

}

console.log("------ Lexical this -----");
person.printFriends();
console.log("------ Lexical this?-----");
//person.anotherPrint();