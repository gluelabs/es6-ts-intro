// includes sostituisce (migliora) indexOf
const string = 'Angular Framework';
console.log(`Test Include = ${string.includes('work')}`); // true

// String.repeat(numberOfRepetitions)
'Angular'.repeat(3); // 'AngularAngularAngular'
console.log(`Test Repeat = ${'Angular'.repeat(3)}`); // true
