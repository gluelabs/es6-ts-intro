console.info('**** REST ARGUMENTS*****');

function printArguments(...args) {
    args.forEach(function(arg) {
        console.log('rest args:', arg)
    });
}

printArguments(1, 2, 3);

console.info('**** SPREAD OPERATOR *****');

function sumEverything(x, y, z) {
    return x + y + z;
}

const nums = [1, 2, 3];
const result = sumEverything(...nums);

console.log("Dati i numeri",nums,` il risultato della somma sumEverything(...nums) = ${result}`);