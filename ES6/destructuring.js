console.info('****DESTRUCTURING*****');

var object = { 'a': 1, 'b': 2 };

var { a, b } = object;
console.log("Ogbetto = ",object);
console.log(`a = ${a} e b = ${b}`);
// a = 1
// b = 2

// Usato principalmente per l'importazione di moduli