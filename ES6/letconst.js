console.info('****** LET and CONST');

/*
 * Entrambe block-scoped.
 *  - let permette la riassegnazione
 *  - const solleva un errore
 *
 */

function f() {
  {
    let x = "first"; 
    {
      const x = "second";
      // error, const
      //x = "foo";
      console.log(`CONSTANT X = ${x}`);

    }
    // error, already declared in block
    //let x = "third";
    console.log(`LET X= ${x}`);
  }
}

f();