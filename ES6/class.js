console.info('******CLASSES******');

class Human {
    constructor(id) {
        this._id = id;
    }
}

class Person extends Human {
    constructor(name, id) {
        super(id);
        this._name = name;
        this._lastname = '<no-surname>';
        console.log(`Built a new Person: ${name} - ${id}`);
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        console.log(`${this._name} wants to change his name in ${newName}`);
        this._name = newName;
        return this;
    }

    static getAnswerToLifeTheUniverseAndEverything() {
        return 42;
    }
}

let _person = new Person('Massimiliano', 1);
_person.name = 'Max'; // Usa automaticamente il setter definito su name

console.log(`What is the answer to Life, Universe and Everything? -> ${Person.getAnswerToLifeTheUniverseAndEverything()}`);