
interface Vehicle {
    type: string;
    wheels: number;
}

class mazda implements Vehicle {

    constructor(public type: string, public wheels: number) { }
}