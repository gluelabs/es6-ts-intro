/** 
 * 
 * A decorator is a function that is going to decorate (populate) a target.
 * A target can be: class, property, method, parameter, accessor (getter or setter)
 * A decorator might look like:
 * 
 */

// For example: 
function clean(target) {
    target.prototype.cleaned = true;
}
function dirty(target) {
    target.prototype.cleaned = false;
}
interface Animal {
    kind: String
}
class AnimalGenerico implements Animal {
    constructor(public kind: String) { };
    isClean = () => {
        return this.cleaned;
    }
    getKind = () =>{
        return this.kind;
    }
}

@clean
class AnimalePulito extends AnimalGenerico {
}

@dirty
class AnimaleSporco extends AnimalGenerico  { }

@clean
@dirty
class AnimaleBho extends AnimalGenerico  { }

function setClean(value: boolean) {
    return function (target) {
        target.prototype.cleaned = value;
    }
}
@setClean(false)
class AnimaleImpostato extends AnimalGenerico  {
}

let animal = new AnimalGenerico("Cane");
let animalePulito = new AnimalePulito("Cane");
let animaleSporco = new AnimaleSporco("Cane");
let animalBho = new AnimaleBho("Cane");
let animaleImpostato = new AnimaleImpostato("Cane");
//TESTING
console.log(`*******TS PLayground`)
console.log(` animal E' Pulito ?? ${animal.isClean()}`);
console.log(` animalePulito E' Pulito  ?? ${animalePulito.isClean()}`);
console.log(` animaleSporco E' Pulito ?? ${animaleSporco.isClean()}`);
console.log(` animalBho?? ${animaleImpostato.isClean()}`);



/**
 * https://www.typescriptlang.org/docs/handbook/decorators.html#decorator-factories
 * 
 * As such, the following steps are performed when evaluating multiple decorators on a single declaration in TypeScript:
 *
 *       The expressions for each decorator are evaluated top-to-bottom.
 *       The results are then called as functions from bottom-to-top.
 * 
 */