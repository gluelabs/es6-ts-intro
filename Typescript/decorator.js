/**
 *
 * A decorator is a function that is going to decorate (populate) a target.
 * A target can be: class, property, method, parameter, accessor (getter or setter)
 * A decorator might look like:
 *
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// For example: 
function clean(target) {
    target.prototype.cleaned = true;
}
function dirty(target) {
    target.prototype.cleaned = false;
}
var AnimalGenerico = (function () {
    function AnimalGenerico(kind) {
        var _this = this;
        this.kind = kind;
        this.isClean = function () {
            return _this.cleaned;
        };
        this.getKind = function () {
            return _this.kind;
        };
    }
    ;
    return AnimalGenerico;
}());
var AnimalePulito = (function (_super) {
    __extends(AnimalePulito, _super);
    function AnimalePulito() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AnimalePulito;
}(AnimalGenerico));
AnimalePulito = __decorate([
    clean
], AnimalePulito);
var AnimaleSporco = (function (_super) {
    __extends(AnimaleSporco, _super);
    function AnimaleSporco() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AnimaleSporco;
}(AnimalGenerico));
AnimaleSporco = __decorate([
    dirty
], AnimaleSporco);
var AnimaleBho = (function (_super) {
    __extends(AnimaleBho, _super);
    function AnimaleBho() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AnimaleBho;
}(AnimalGenerico));
AnimaleBho = __decorate([
    clean,
    dirty
], AnimaleBho);
function setClean(value) {
    return function (target) {
        target.prototype.cleaned = value;
    };
}
var AnimaleImpostato = (function (_super) {
    __extends(AnimaleImpostato, _super);
    function AnimaleImpostato() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AnimaleImpostato;
}(AnimalGenerico));
AnimaleImpostato = __decorate([
    setClean(false)
], AnimaleImpostato);
var animal = new AnimalGenerico("Cane");
var animalePulito = new AnimalePulito("Cane");
var animaleSporco = new AnimaleSporco("Cane");
var animalBho = new AnimaleBho("Cane");
var animaleImpostato = new AnimaleImpostato("Cane");
//TESTING
console.log("*******TS PLayground");
console.log(" animal E' Pulito ?? " + animal.isClean());
console.log(" animalePulito E' Pulito  ?? " + animalePulito.isClean());
console.log(" animaleSporco E' Pulito ?? " + animaleSporco.isClean());
console.log(" animalBho?? " + animaleImpostato.isClean());
/**
 * https://www.typescriptlang.org/docs/handbook/decorators.html#decorator-factories
 *
 * As such, the following steps are performed when evaluating multiple decorators on a single declaration in TypeScript:
 *
 *       The expressions for each decorator are evaluated top-to-bottom.
 *       The results are then called as functions from bottom-to-top.
 *
 */ 
